// pages/search/index.js
import { request } from "../../request/index.js";
import regeneratorRuntime, { async } from '../../lib/runtime/runtime';
Page({

    /**
     * 页面的初始数据
     */
    data: {
        goods:[],
        isFocus:false,
        inValue:""

    },
   TimId:-1,
    handleInput(e){
    const {value}=e.detail
    if(!value.trim()){
        this.setData({
            goods:[],
            isFocus:false
        })
        return
    }
    this.setData({
        isFocus:true
    })
    clearTimeout(this.TimId)
    this.TimId=setTimeout(()=>{
 this.qsearch(value) 
    },1000)
     
    },
    async qsearch(query){
      const res = await request({
          url:"https://api-hmugo-web.itheima.net/api/public/v1/goods/search"
         ,data:{query}
      })
 
     this.setData({
         goods:res.goods
     })
    },
    handleCancel(){
        
        this.setData({
            goods:[],
            isFocus:false,
            inValue:""
        })
    }
})