//Page Object
//一定要把request封装成一个对象，本来在里面就是对象
import {request} from "../../request/index.js"
import regeneratorRuntime from '../../lib/runtime/runtime'
Page({
  data: {
    swiperList:[],
    catesList:[],
    floorList:[]
  },
  //options(Object)
  onLoad: function(options) {
   this.getSwiperList();  
   this. getCatesList();
   this.getFloorList();
  },
   async getSwiperList(){
     const res=await request({url:"https://api-hmugo-web.itheima.net/api/public/v1/home/swiperdata"})
   
      this.setData({
        swiperList:res
      })
  
  },
   async getCatesList(){
    const res=await request({url:"https://api-hmugo-web.itheima.net/api/public/v1/home/catitems"})
   
      this.setData({
        catesList:res
      })
    
  },
  async getFloorList(){
     const res=await request({url:"https://api-hmugo-web.itheima.net/api/public/v1/home/floordata"})

      this.setData({
        floorList:res
      })
      },
});
  