// pages/category/index.js
import {request} from "../../request/index.js"
import regeneratorRuntime from '../../lib/runtime/runtime'
Page({
  /**
     * 页面的初始数据
     */
    data: {
   Cates:[],
   leftMenuList:[],
   rightContent:[],
   currentIndex:0
    },

    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
       const Cates=wx.getStorageSync("cates");
         if(!Cates){
             this.getCates();
         }else{
           if(new Date()-Cates.time>1000*10){
               this.getCates();
           }else{
             this.Cates=Cates.data
              let leftMenuList=this.Cates.map(v=>v.cat_name)
              let rightContent=this.Cates[0].children
              this.setData({
                leftMenuList,
                rightContent,
                Cates:Cates.data.message
              })
           }
         }
    },
    async getCates(){
    //     request({
    //         url:"https://api-hmugo-web.itheima.net/api/public/v1/categories"
    //     })
    //     .then(res=>{
    //    this.Cates=res.data.message;
    //    wx.setStorageSync("cates", {time:new Date(),data:this.Cates});
         
    //    //缓存，不出现在Appdata里面
    //    let leftMenuList=this.Cates.map(v=>v.cat_name)
    //    let rightContent=this.Cates[0].children
    //    this.setData({
    //      leftMenuList,
    //      rightContent,
        
    //    })
    // })
    const res=await request({url:"https://api-hmugo-web.itheima.net/api/public/v1/categories"})
      this.Cates=res;
       wx.setStorageSync("cates", {time:new Date(),data:this.Cates});
         
       //缓存，不出现在Appdata里面
       let leftMenuList=this.Cates.map(v=>v.cat_name)
       let rightContent=this.Cates[0].children
       this.setData({
         leftMenuList,
         rightContent,
        
       })   
  },
    handleItemTap(e){
    const {index}=e.currentTarget.dataset;
    let rightContent = this.Cates[index].children;
    this.setData({
      currentIndex:index,
      rightContent
    })
    }
})