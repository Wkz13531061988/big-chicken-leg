// pages/cart/index.js
import regeneratorRuntime from '../../lib/runtime/runtime'
import {request} from '../../request/index.js'
import {getSetting,chooseAddress,openSetting,showModal,showToast} from '../../utils/asyncWx'
Page({
    data:{
        address:{},
        cart:[],
      
        totalPrice:0,
        totalNum:0,
    },
    onShow(){
     const address=wx.getStorageSync("address");
     let cart =wx.getStorageSync("cart")||[];
     cart=cart.filter(v=>v.checked)
    
       this.setData({
           address,
       })
     let totalPrice=0;
     let totalNum=0;
     
     cart.forEach(v=> {
       
            totalPrice+=v.num*v.goods_price
            totalNum+=v.num

     });
     this.setData({
         cart,
         totalPrice,
         totalNum,
        
     })
      wx.setStorageSync("cart", cart);
    },
   async handleOrderPay(){
        const token=wx.getStorageSync("token");
        if(!token){
            wx.navigateTo({
                url: '/pages/auth/index',
              
            });
             
        }else{
            wx.showToast({
                title: '后台不行了，即将跳回首页，宝！',
                icon: 'none',
                  duration: 2000,
                mask: true,
            });
              setTimeout(() => {
                  wx.switchTab({
                      url: '../index/index',
                  });
                    
                  
              }, 2000);
              
              
        }
        // const header={Authorization:token}
        // const order_price=this.data.totalPrice
        // const consignee_addr =this.data.address.all
        // const cart=this.data.cart
        // let goods=[]
        // cart.forEach(v=>goods.push({
        //     goods_id:v.goods_id,
        //     goods_number:v.num,
        //     goods_price:v.goods_price

        // }))
        // const orderParams={order_price,consignee_addr,cart}
        // const res=await request({
        //     url:"https://api-hmugo-web.itheima.net/api/public/v1/my/orders/create",
        //     methods: "post",
        //     data:orderParams,
        //     header:header
        // })
        // console.log(res);
          
    }


  

})