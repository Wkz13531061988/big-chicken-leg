
let ajaxTimes=0;
export const request=(params)=>{
    // const baseUrl="https://api-hmugo-web.itheima.net/api/public/v1",
    //接口公共部位不同
    ajaxTimes++
    wx.showLoading({
        title: "等一会宝",
        mask: true,
        //mask为遮面成
    });
      
    return new Promise((resolve,reject)=>{
        wx.request({
            ...params,
            success: (res) => {
                resolve(res.data.message)
            },
            fail: (err) => {
                reject(err)
            },
            complete:()=>{
                ajaxTimes--
                if(ajaxTimes===0){
                    wx.hideLoading() 
                }
            
            }
        });
          
    })
}