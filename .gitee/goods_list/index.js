// pages/goods_list/index.js
import regeneratorRuntime from '../../lib/runtime/runtime'
import { request } from "../../request/index.js";
Page({

    /**
     * 页面的初始数据
     */
    data: {
      tabs:[
          {
              id:0,
              value:"综合",
              isActive:true
          },
           {
              id:1,
              value:"销量",
              isActive:false
          },
           {
              id:2,
              value:"价格",
              isActive:false
          },
      ],
      goodsList:[],
    },
    //对全局事件每次请求的数值进行概括,query是发出的类型，cid是号码，pagenum为当前页码， pagesize每页取得的数据数
    QueryParams:{
        query:"",
        cid:"",
        pagenum:1,
        pagesize:10
    },
    totalPages:1,
    /**
     * 生命周期函数--监听页面加载
     */
    onLoad: function (options) {
      this.QueryParams.cid=options.cid||"";
      this.QueryParams.query=options.query||"";

      this.getGoodsList()
        
    },
   async getGoodsList(){
       const res=await request({url:"https://api-hmugo-web.itheima.net/api/public/v1/goods/search",data:this.QueryParams})
       const total=res.total;
       this.totalPages=Math.ceil(total/this.QueryParams.pagesize)
       this.setData({
           goodsList:[...this.data.goodsList,...res.goods]
       })    
       wx.stopPullDownRefresh()
         
    },
    handleTabsItemChange(e){
        const {index}=e.detail
        let {tabs}=this.data
        tabs.forEach((v,i) =>i===index?v.isActive=true:v.isActive=false);
        this.setData({
            tabs
        })
    },
    onReachBottom(){
        if(this.QueryParams.pagenum>=this.totalPages){
         wx.showToast({
             title: '没有了宝',
         });
           
        }else{
            this.QueryParams.pagenum++;
            this.getGoodsList();
        }
    },
    onPullDownRefresh(){
        this.setData({
             goodsList:[]
        })
         this.QueryParams.pagenum=1;
         this.getGoodsList()

    }

})